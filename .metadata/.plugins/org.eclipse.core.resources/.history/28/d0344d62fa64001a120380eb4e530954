package com.medical.shop.management.system.Medical.Managment.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.medical.shop.management.system.Medical.Managment.Dao.SupplierDaoImpl;
import com.medical.shop.management.system.Medical.Managment.Model.Product;
import com.medical.shop.management.system.Medical.Managment.Model.Supplier;

@Controller
public class SupplierController {
	@Autowired
	SupplierDaoImpl supplierdaoimpl;

	// To display list of products order by suppliers
	@ModelAttribute("supplierwiseList")
	public List<Product> supplierwiseList() {
		return supplierdaoimpl.getSupplierWiseList();
	}

	// Display list of all the suppliers
	@ModelAttribute("supplierName")
	public List<String> supplierNameList() {
		return supplierdaoimpl.getSupplierNameList();
	}

	// Supplier Wise Report Controller
	@RequestMapping(value = "/supplierList", method = RequestMethod.GET)
	public String supplierList(@ModelAttribute("supplier") Supplier supplier, ModelMap model) {
		return "supplierWiseList";
	}

	// Update Supplier Controller
	@RequestMapping(value = "/editSupplierDetails", method = RequestMethod.GET)
	public String editSuppliersDisplay(@ModelAttribute("supplier") Supplier supplier) {
		return "updateSupplier";
	}

	@RequestMapping(value = "/editSupplierDetails", method = RequestMethod.POST)
	public String editSuppliers(@ModelAttribute("supplier") Supplier supplier, ModelMap model) {
		supplier = supplierdaoimpl.getSupplierByName(supplier.getSupplier_name());
		model.put("supplier", supplier);
		model.put("button", "Update");
		return "AddNewSupplier";
	}

	@RequestMapping(value = "/UpdateNewSupplier", method = RequestMethod.POST)
	public String editNew(@ModelAttribute("supplier") Supplier supplier, BindingResult result, ModelMap model) {
		System.out.println(supplier);
		if (!result.hasErrors() && supplierdaoimpl.updateSupplier(supplier)) {
			model.put("message", "Supplier Details Updated Successfully");
		} else {
			model.put("message", "Unsuccessful");
		}
		model.put("button", "Update");
		return "manageSupplier";
	}

	// Add Supplier Controller
	@RequestMapping(value = "/AddNewSupplier", method = RequestMethod.GET)
	public String addNewSupplier(@ModelAttribute("supplier") Supplier supplier, ModelMap model) {
		model.put("button", "Add");
		return "AddNewSupplier";
	}

	@RequestMapping(value = "/AddNewSupplier", method = RequestMethod.POST)
	public String addNew(@ModelAttribute("supplier") @Valid Supplier supplier, BindingResult result, ModelMap model) {
		if(supplierdaoimpl.getSupplierByName(supplier.getSupplier_name()) != null) {
			model.put("message", "Supplier Already Exist");
			model.put("button", "Add");
			return "AddNewSupplier"; 
		}
		if (!result.hasErrors() && supplierdaoimpl.addNewSupplier(supplier)) {
			model.put("message", "Supplier Details Added Successfully");
		} else {
			model.put("message", "Unsuccessful");
		}
		model.put("button", "Add");
		return "manageSupplier";
	}

	// Delete Supplier Controller
	@RequestMapping(value = "/deleteSupplier", method = RequestMethod.GET)
	public String deleteSupplier(@ModelAttribute("supplier") Supplier supplier) {
		return "deleteSupplier";
	}

	@RequestMapping(value = "/deleteSupplier", method = RequestMethod.POST)
	public String deleteSupplierDB(@ModelAttribute("supplier") Supplier supplier, BindingResult result,
			ModelMap model) {

		if (!result.hasErrors() && supplierdaoimpl.deleteSupplier(supplier.getSupplier_name())) {
			model.put("message", "Supplier Details Deleted Successfully");
			return "manageSupplier";
		}
		model.put("message", "Unsuccessful");

		return "manageSupplier";
	}

	// Manage Supplier Controller to display all options to manage supplier
	@RequestMapping(value = "/manageSupplier", method = RequestMethod.GET)
	public String manageSupplier() {
		return "manageSupplier";
	}

}
