<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<style>
body {
	background-image:
		url("https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77701450862.jpg");
	background-repeat: no-repeat;
	background-position: fixed;
	background-size: cover;
	color: white;
}
</style>
</head>

<body>

	<!-- Navbar -->


	<nav class="navbar navbar-expand-lg navbar-dark bg-dark"> <a
		class="navbar-brand" href="/options">Medic</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link" href="/options">Home
					<span class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
			<li class="nav-item"><a class="nav-link" href="#">Contact Us</a>
			</li>
		</ul>
	</div>
	<a href = "/reset" class = "btn btn-outline-primary">Reset Password</a>&nbsp;&nbsp;
	<form:form class="form-inline" action="/logout" method="post">

		<button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Logout</button>
	</form:form> </nav>

	<!-- Navbar -->



<c:if test="${expiry !=null}">

		<div class="row">
			<div class="col-3"></div>
			<div class="col-4">
			<center>
				<h5 class = "m-2">Notice!!!</h5>
				<h6>Medicines about to expired</h6></center>
				<table class="table bg-light text-dark">
					<thead>
						<th scope="col">Medicine Name</th>
						<th scope="col">Expiry Date</th>
					</thead>
					<tbody>
						<c:forEach var="product" items="${expiry}">
							<tr>
								<td>${product.getKey()}</td>
								<td>${product.getValue()}</td>
							</tr>
						</c:forEach>

					</tbody>

				</table>

			</div>
		</div>

	</c:if>


	<div class="row p-4">
		<div class="col-2"></div>
		<div class="card bg-dark col-2 m-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQTqF3tsyXSQLKsU9OqWJ8sWTnKW5Nxf1pQMkH6_Y-DkkH_2Ehk"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Product Search</h5>
				<p class="card-text"> </p>
				<a href="/productsearch" class="btn btn-primary">Click Here</a>
			</div></center>
		</div>
		<div class="card bg-dark m-2 col-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://i.pinimg.com/236x/c9/63/1f/c9631fbc305901402dae0b594543d213.jpg"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Stock Maintenance</h5>
				<p class="card-text"> </p>
				<a href="/stockMaintenance" class="btn btn-primary">Click Here</a>
			</div>
			</center>
		</div>
		<div class="card bg-dark col-2 m-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRLvUbDlKiE0f0PiTsKgtjdic4CHxTWBJclDotUSPoGWoJpvYGW"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Purchase Report</h5>
				<p class="card-text"></p>
				<a href="/purchaseReport" class="btn btn-primary">Click Here</a>
			</div>
			</center>
		</div>
	</div>

	<div class="row p-4">
		<div class="col-2"></div>
		<div class="card bg-dark m-2 col-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSUMPBTqu1l6S3x-ShvoK-hAnFu__aEXT23JK4P0oK1GpxHMwbv"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Supplier Wise List</h5>
				<p class="card-text"> </p>
				<a href="/supplierList" class="btn btn-primary">Click Here</a>
			</div>
			</center>
		</div>

		<div class="card bg-dark col-2 m-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT5epGPAYeEMyxTMUW-BPsEv8WOtGn0hkzm8kZGGMmMymsPl3bd"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Manage Product</h5>
				<p class="card-text"> </p>
				<a href="/manageProduct" class="btn btn-primary">Click Here</a>
			</div>
			</center>
		</div>

		<div class="card bg-dark m-2 col-2" style="width: 18rem;">
			<img class="card-img-top"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ6gmd6XhWZy-aVz4m8JeqFEQ4Cez6xqGVnB4HfvOPtYlqAxymL"
				alt="Card image cap">
				<center>
			<div class="card-body">
				<h5 class="card-title">Manage Supplier</h5>
				<p class="card-text"> </p>
				<a href="/manageSupplier" class="btn btn-primary">Click Here</a>
			</div>
			</center>
		</div>
	</div>




	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>
