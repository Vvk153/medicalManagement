package com.medical.shop.management.system.Medical.Managment.Controller;

import java.util.Date;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.medical.shop.management.system.Medical.Managment.Dao.ProductDaoImpl;
import com.medical.shop.management.system.Medical.Managment.Dao.UserDaoImpl;
import com.medical.shop.management.system.Medical.Managment.GateWay.SMSManager;
import com.medical.shop.management.system.Medical.Managment.MailServer.SimpleMailManager;
import com.medical.shop.management.system.Medical.Managment.Model.Users;

@Controller
@SessionAttributes("admin")
public class UserController {

	@Autowired
	UserDaoImpl userDaoImpl;

	@Autowired
	ProductDaoImpl product;

	// Registration Controller
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String userRegistrationDisplay(@ModelAttribute("users") Users user) {
		return "Registration_page";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String userRegistration(@ModelAttribute("users") @Valid Users user, BindingResult result, ModelMap model) {
		// System.out.println(userDaoImpl.userExist(user.getUser_id()));
		if (result.hasErrors()) {
			return "Registration_page";
		}
		if (userDaoImpl.addUser(user)) {
			model.put("status", "Registeration done Successfully");
		} else {
			model.put("status", "Username is already used");
		}
		System.out.println();
		return "Registration_page";
	}

	// Login display Controller
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String userLoginDisplay() {
		return "login";
	}

	// Options to successful login controller
	@RequestMapping(value = "/options", method = RequestMethod.GET)
	public String successUserLoginDisplay(ModelMap model) {
		Map<String, Date> m = product.expiryAlarm();
		if (m.isEmpty()) {
			m = null;
		}
		model.addAttribute("expiry", m);

		return "options";
	}

	// Forgot password controller
	@RequestMapping(value = "/forgot", method = RequestMethod.GET)
	public String forgetPassword(@ModelAttribute("users") Users users) {
		return "forgotPassword";
	}

	@RequestMapping(value = "/forgot", method = RequestMethod.POST)
	public String sendEmailOrMsg(@ModelAttribute("users") Users user, ModelMap model) {
		if (user.getContactNo().equals("") && user.getEmailId().equals("")) {
			model.put("msg", "Please,fill atleast one");
			return "forgotPassword";
		}
		System.out.println(user.getContactNo());
		if (user.getContactNo().equals("")) {
			SimpleMailManager mail = new SimpleMailManager();
			user = userDaoImpl.getUserByEmailId(user.getEmailId());
			System.out.println(user);
			if (mail.sendMessage(user)) {
				model.put("msg", "Your username and password has succesfully sent to your email address");
			} else {
				model.put("msg", "Server Error!\n try after some time");

			}
		} else {
			SMSManager sms = new SMSManager();
			user = userDaoImpl.getUserByContact(user.getContactNo());
			if (sms.sendSMS(user)) {
				model.put("msg", "Your username and password has succesfully sent to your mobile number");
			} else {
				model.put("msg", "Server Error!\n try after some time");
			}
		}

		return "forgotPassword";
	}

	// Change Password Controller
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String changePasswordDisplay() {
		return "changePassword";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public String changePassword(@RequestParam String currentPassword, @RequestParam String newPassword,
			@RequestParam String rePassword, ModelMap model) {
		String username = getUsernameFromSession();
		String dbStorPassword = userDaoImpl.getPasswordByUserName(username);
		System.out.println(dbStorPassword + " ");
		if (!newPassword.equals(rePassword)) {
			model.put("error", "New Password & Re-Password are not same");
		} else if (newPassword.length() < 8 || newPassword.length() > 64) {
			model.put("error", "New Password must be of length between 8 to 64 character");
		} else if (dbStorPassword == null || !dbStorPassword.equals("{noop}" + currentPassword)) {
			model.put("error", "Current Password is not correct");
		} else if (userDaoImpl.changePassword(username, newPassword)) {
			model.put("error", "Password Changed Successfully");
		} else {
			model.put("error", "Password Not Updated");
		}
		return "changePassword";
	}

	// Method to fetch username from current login session
	public String getUsernameFromSession() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			return ((UserDetails) principal).getUsername();
		}
		return principal.toString();
	}
}
